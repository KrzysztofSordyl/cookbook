namespace Cookbook.CookbookDatabase
{
    using Cookbook.Models;
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class CookbookDbContext : DbContext
    {
        public CookbookDbContext()
            : base("name=CookbookDbContext")
        {
        }

        public virtual DbSet<Recipe> Recipes { get; set; }

        public virtual DbSet<RecipePoint> RecipePoints { get; set; }

        public virtual DbSet<Ingredient> Ingredients { get; set; }

        public virtual DbSet<RecipesIngredientAmount> RecipesIngredientAmounts { get; set; }

        public virtual DbSet<IngredientsGroup> IngredientsGroups { get; set; }

        public virtual DbSet<IngredientsGroupsIngredientAmount> IngredientsGroupsIngredientAmounts { get; set; }

        public virtual DbSet<Unit> Units { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}