namespace Cookbook.CookbookDatabase.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Ingredients",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        BaseIngredientId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Ingredients", t => t.BaseIngredientId)
                .Index(t => t.BaseIngredientId);
            
            CreateTable(
                "dbo.IngredientsGroupsIngredientAmounts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IngredientsGroupId = c.Int(nullable: false),
                        Amount = c.Double(),
                        AmountMin = c.Double(),
                        AmountMax = c.Double(),
                        UnitId = c.Int(nullable: false),
                        IngredientId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Ingredients", t => t.IngredientId, cascadeDelete: true)
                .ForeignKey("dbo.IngredientsGroups", t => t.IngredientsGroupId, cascadeDelete: true)
                .ForeignKey("dbo.Units", t => t.UnitId, cascadeDelete: true)
                .Index(t => t.IngredientsGroupId)
                .Index(t => t.UnitId)
                .Index(t => t.IngredientId);
            
            CreateTable(
                "dbo.IngredientsGroups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        RecipeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Recipes", t => t.RecipeId, cascadeDelete: true)
                .Index(t => t.RecipeId);
            
            CreateTable(
                "dbo.Recipes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.RecipesIngredientAmounts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RecipeId = c.Int(nullable: false),
                        Amount = c.Double(),
                        AmountMin = c.Double(),
                        AmountMax = c.Double(),
                        UnitId = c.Int(nullable: false),
                        IngredientId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Ingredients", t => t.IngredientId, cascadeDelete: true)
                .ForeignKey("dbo.Recipes", t => t.RecipeId, cascadeDelete: true)
                .ForeignKey("dbo.Units", t => t.UnitId, cascadeDelete: true)
                .Index(t => t.RecipeId)
                .Index(t => t.UnitId)
                .Index(t => t.IngredientId);
            
            CreateTable(
                "dbo.Units",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.RecipePoints",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Number = c.Int(nullable: false),
                        Text = c.String(nullable: false),
                        RecipeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Recipes", t => t.RecipeId, cascadeDelete: true)
                .Index(t => t.RecipeId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Ingredients", "BaseIngredientId", "dbo.Ingredients");
            DropForeignKey("dbo.IngredientsGroupsIngredientAmounts", "UnitId", "dbo.Units");
            DropForeignKey("dbo.IngredientsGroupsIngredientAmounts", "IngredientsGroupId", "dbo.IngredientsGroups");
            DropForeignKey("dbo.IngredientsGroups", "RecipeId", "dbo.Recipes");
            DropForeignKey("dbo.RecipePoints", "RecipeId", "dbo.Recipes");
            DropForeignKey("dbo.RecipesIngredientAmounts", "UnitId", "dbo.Units");
            DropForeignKey("dbo.RecipesIngredientAmounts", "RecipeId", "dbo.Recipes");
            DropForeignKey("dbo.RecipesIngredientAmounts", "IngredientId", "dbo.Ingredients");
            DropForeignKey("dbo.IngredientsGroupsIngredientAmounts", "IngredientId", "dbo.Ingredients");
            DropIndex("dbo.RecipePoints", new[] { "RecipeId" });
            DropIndex("dbo.RecipesIngredientAmounts", new[] { "IngredientId" });
            DropIndex("dbo.RecipesIngredientAmounts", new[] { "UnitId" });
            DropIndex("dbo.RecipesIngredientAmounts", new[] { "RecipeId" });
            DropIndex("dbo.IngredientsGroups", new[] { "RecipeId" });
            DropIndex("dbo.IngredientsGroupsIngredientAmounts", new[] { "IngredientId" });
            DropIndex("dbo.IngredientsGroupsIngredientAmounts", new[] { "UnitId" });
            DropIndex("dbo.IngredientsGroupsIngredientAmounts", new[] { "IngredientsGroupId" });
            DropIndex("dbo.Ingredients", new[] { "BaseIngredientId" });
            DropTable("dbo.RecipePoints");
            DropTable("dbo.Units");
            DropTable("dbo.RecipesIngredientAmounts");
            DropTable("dbo.Recipes");
            DropTable("dbo.IngredientsGroups");
            DropTable("dbo.IngredientsGroupsIngredientAmounts");
            DropTable("dbo.Ingredients");
        }
    }
}
