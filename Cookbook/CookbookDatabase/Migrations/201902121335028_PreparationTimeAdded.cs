namespace Cookbook.CookbookDatabase.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PreparationTimeAdded : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Recipes", "PreparationTime", c => c.Int(nullable: false));
            AddColumn("dbo.RecipePoints", "PreparationTime", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.RecipePoints", "PreparationTime");
            DropColumn("dbo.Recipes", "PreparationTime");
        }
    }
}
