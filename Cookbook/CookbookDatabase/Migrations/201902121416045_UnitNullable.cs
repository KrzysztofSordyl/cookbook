namespace Cookbook.CookbookDatabase.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UnitNullable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.IngredientsGroupsIngredientAmounts", "UnitId", "dbo.Units");
            DropForeignKey("dbo.RecipesIngredientAmounts", "UnitId", "dbo.Units");
            DropIndex("dbo.IngredientsGroupsIngredientAmounts", new[] { "UnitId" });
            DropIndex("dbo.RecipesIngredientAmounts", new[] { "UnitId" });
            AlterColumn("dbo.IngredientsGroupsIngredientAmounts", "UnitId", c => c.Int());
            AlterColumn("dbo.RecipesIngredientAmounts", "UnitId", c => c.Int());
            CreateIndex("dbo.IngredientsGroupsIngredientAmounts", "UnitId");
            CreateIndex("dbo.RecipesIngredientAmounts", "UnitId");
            AddForeignKey("dbo.IngredientsGroupsIngredientAmounts", "UnitId", "dbo.Units", "Id");
            AddForeignKey("dbo.RecipesIngredientAmounts", "UnitId", "dbo.Units", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.RecipesIngredientAmounts", "UnitId", "dbo.Units");
            DropForeignKey("dbo.IngredientsGroupsIngredientAmounts", "UnitId", "dbo.Units");
            DropIndex("dbo.RecipesIngredientAmounts", new[] { "UnitId" });
            DropIndex("dbo.IngredientsGroupsIngredientAmounts", new[] { "UnitId" });
            AlterColumn("dbo.RecipesIngredientAmounts", "UnitId", c => c.Int(nullable: false));
            AlterColumn("dbo.IngredientsGroupsIngredientAmounts", "UnitId", c => c.Int(nullable: false));
            CreateIndex("dbo.RecipesIngredientAmounts", "UnitId");
            CreateIndex("dbo.IngredientsGroupsIngredientAmounts", "UnitId");
            AddForeignKey("dbo.RecipesIngredientAmounts", "UnitId", "dbo.Units", "Id", cascadeDelete: true);
            AddForeignKey("dbo.IngredientsGroupsIngredientAmounts", "UnitId", "dbo.Units", "Id", cascadeDelete: true);
        }
    }
}
