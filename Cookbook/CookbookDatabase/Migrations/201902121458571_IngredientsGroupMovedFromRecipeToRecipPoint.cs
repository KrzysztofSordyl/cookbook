namespace Cookbook.CookbookDatabase.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IngredientsGroupMovedFromRecipeToRecipPoint : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.IngredientsGroups", "RecipeId", "dbo.Recipes");
            DropIndex("dbo.IngredientsGroups", new[] { "RecipeId" });
            AddColumn("dbo.IngredientsGroups", "RecipePointId", c => c.Int(nullable: false));
            CreateIndex("dbo.IngredientsGroups", "RecipePointId");
            AddForeignKey("dbo.IngredientsGroups", "RecipePointId", "dbo.RecipePoints", "Id", cascadeDelete: true);
            DropColumn("dbo.IngredientsGroups", "RecipeId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.IngredientsGroups", "RecipeId", c => c.Int(nullable: false));
            DropForeignKey("dbo.IngredientsGroups", "RecipePointId", "dbo.RecipePoints");
            DropIndex("dbo.IngredientsGroups", new[] { "RecipePointId" });
            DropColumn("dbo.IngredientsGroups", "RecipePointId");
            CreateIndex("dbo.IngredientsGroups", "RecipeId");
            AddForeignKey("dbo.IngredientsGroups", "RecipeId", "dbo.Recipes", "Id", cascadeDelete: true);
        }
    }
}
