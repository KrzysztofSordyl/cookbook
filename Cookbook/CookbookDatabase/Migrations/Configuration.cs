namespace Cookbook.CookbookDatabase.Migrations
{
    using Cookbook.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Cookbook.CookbookDatabase.CookbookDbContext>
    {
        public Configuration()
        {
            this.AutomaticMigrationsEnabled = false;
            this.MigrationsDirectory = "CookbookDatabase\\Migrations";
        }

        protected override void Seed(CookbookDbContext context)
        {
            // From my mother's cookbook:
            Unit glass = new Unit() { Name = "szkl." };

            Ingredient apple = new Ingredient() { Name = "Jab�ko" };
            Ingredient cinnamon = new Ingredient() { Name = "Cynamon" };
            Ingredient sugar = new Ingredient() { Name = "Cukier" };
            Ingredient flour = new Ingredient() { Name = "M�ka" };
            Ingredient bakingPowder = new Ingredient() { Name = "Proszek do pieczenia" };
            Ingredient soda = new Ingredient() { Name = "Soda" };
            Ingredient egg = new Ingredient() { Name = "Jajko" };

            Recipe cake = new Recipe()
            {
                Id = 1,
                Name = "Placek z cynamonem i jab�kami",
                PreparationTime = 180,
                IngredientAmounts = new List<RecipesIngredientAmount>
                {
                    new RecipesIngredientAmount()
                    {
                        Amount = 8,
                        Ingredient = apple
                    },
                    new RecipesIngredientAmount()
                    {
                        Amount = 0,
                        Ingredient = cinnamon
                    },
                    new RecipesIngredientAmount()
                    {
                        Amount = 1.5,
                        Unit = glass,
                        Ingredient = sugar
                    },
                    new RecipesIngredientAmount()
                    {
                        Amount = 3,
                        Unit = glass,
                        Ingredient = flour
                    },
                    new RecipesIngredientAmount()
                    {
                        Amount = 0,
                        Ingredient = bakingPowder
                    },
                    new RecipesIngredientAmount()
                    {
                        Amount = 0,
                        Ingredient = soda
                    },
                    new RecipesIngredientAmount()
                    {
                        Amount = 3,
                        Ingredient = egg
                    }
                },
                Points = new List<RecipePoint>
                {
                    new RecipePoint()
                    {
                        Number = 1,
                        Text = "Obrane jab�ka pokroi�� w kostk�, doda� cynamon, cukier i wymiesza�."
                    },
                    new RecipePoint()
                    {
                        Number = 2,
                        Text = "Odstawi� na 1-2 godziny."
                    },
                    new RecipePoint()
                    {
                        Number = 3,
                        Text = "Nast�pnie doda� rozmieszane jajka i wymiesza�. M�k� z proszkiem i sod� wymiesza� z jab�kami. Wy�o�y� na blach� i piec do zbr�zowienia ciasta."
                    }
                }
            };

            Unit spoon = new Unit() { Name = "�y�ka" };

            Ingredient water = new Ingredient() { Name = "Woda" };
            Ingredient margarine = new Ingredient() { Name = "Margaryna" };
            Ingredient salt = new Ingredient() { Name = "S�l" };
            Ingredient milk = new Ingredient() { Name = "Mleko" };
            Ingredient vanillaSugar = new Ingredient() { Name = "Cukier waniliowy" };
            Ingredient wheatFlour = new Ingredient() { Name = "M�ka pszenna", BaseIngredient = flour };
            Ingredient potatoFlour = new Ingredient() { Name = "M�ka ziemniaczana", BaseIngredient = flour };
            Ingredient powderedSugar = new Ingredient() { Name = "Cukier puder" };

            Recipe karpatka = new Recipe()
            {
                Id = 2,
                Name = "Karpatka",
                PreparationTime = 140,
                IngredientAmounts = new List<RecipesIngredientAmount>
                {
                    new RecipesIngredientAmount()
                    {
                        Amount = 1,
                        Unit = glass,
                        Ingredient = flour
                    },
                    new RecipesIngredientAmount()
                    {
                        Amount = 4,
                        Ingredient = egg
                    },
                    new RecipesIngredientAmount()
                    {
                        Amount = 1,
                        Ingredient = margarine
                    },
                    new RecipesIngredientAmount()
                    {
                        Amount = 0,
                        Ingredient = powderedSugar
                    }
                },
                Points = new List<RecipePoint>
                {
                    new RecipePoint()
                    {
                        Number = 1,
                        Name = "Ciasto",
                        Text = "Do gor�cego wsypa� 1 szkl. m�ki i wymiesza�. Wla� po jednym 4 jajka i uciera�. Upiec 2 placki w temp. 200 stopni.",
                        IngredientsGroups = new List<IngredientsGroup>
                        {
                            new IngredientsGroup()
                            {
                                Name = "Zagotowa�",
                                IngredientsAmounts = new List<IngredientsGroupsIngredientAmount>
                                {
                                    new IngredientsGroupsIngredientAmount()
                                    {
                                        Amount = 1,
                                        Unit = glass,
                                        Ingredient = water
                                    },
                                    new IngredientsGroupsIngredientAmount()
                                    {
                                        Amount = 0.5,
                                        Ingredient = margarine
                                    },
                                    new IngredientsGroupsIngredientAmount()
                                    {
                                        Amount = 0,
                                        Ingredient = salt
                                    }
                                }
                            }
                        }
                    },
                    new RecipePoint()
                    {
                        Number = 2,
                        Name = "Masa",
                        Text = "W 1/2 szklanki mleka rozpu�ci� po 1 �y�ce m�ki pszennej i ziemniaczanej. Wla� na gotuj�ce si� mleko. Powsta�y budy� ostudzi�. Utrze� 1 margaryn� lub mas�o i dodawa� powoli budy� ucieraj�c.",
                        IngredientsGroups = new List<IngredientsGroup>
                        {
                            new IngredientsGroup()
                            {
                                Name = "Zagotowa�",
                                IngredientsAmounts = new List<IngredientsGroupsIngredientAmount>
                                {
                                    new IngredientsGroupsIngredientAmount()
                                    {
                                        Amount = 1,
                                        Unit = glass,
                                        Ingredient = milk
                                    },
                                    new IngredientsGroupsIngredientAmount()
                                    {
                                        Amount = 8,
                                        Unit = spoon,
                                        Ingredient = sugar
                                    },
                                    new IngredientsGroupsIngredientAmount()
                                    {
                                        Amount = 0,
                                        Ingredient = vanillaSugar
                                    }
                                }
                            },
                            new IngredientsGroup()
                            {
                                Name = "Wymiesza�",
                                IngredientsAmounts = new List<IngredientsGroupsIngredientAmount>
                                {
                                    new IngredientsGroupsIngredientAmount()
                                    {
                                        Amount = 0.5,
                                        Unit = glass,
                                        Ingredient = milk
                                    },
                                    new IngredientsGroupsIngredientAmount()
                                    {
                                        Amount = 1,
                                        Unit = spoon,
                                        Ingredient = wheatFlour
                                    },
                                    new IngredientsGroupsIngredientAmount()
                                    {
                                        Amount = 1,
                                        Unit = spoon,
                                        Ingredient = potatoFlour
                                    }
                                }
                            }
                        }
                    },
                    new RecipePoint()
                    {
                        Number = 3,
                        Text = "Wy�o�y� mas� na placek i przykry� drugim. Posypa� cukrem pudrem."
                    }
                }
            };

            context.Recipes.AddOrUpdate(cake);
            context.Recipes.AddOrUpdate(karpatka);
            context.SaveChanges();
        }
    }
}
