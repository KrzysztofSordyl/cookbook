﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cookbook.CustomValidators
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = true)]
    public class XnorRequired : ValidationAttribute
    {
        public XnorRequired(string property1, string property2)
        {
            this.property1 = property1;
            this.property2 = property2;
        }

        private readonly string property1;
        private readonly string property2;

        public override bool RequiresValidationContext => true;

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            return validationContext.ObjectType
                .GetProperty(this.property1)
                .GetValue(value) == null ^
                validationContext.ObjectType
                .GetProperty(this.property2)
                .GetValue(value) == null
                ? new ValidationResult(
                    $"Exactly one of properties: {this.property1} and {this.property2} must be null.")
                : ValidationResult.Success;
        }
    }
}
