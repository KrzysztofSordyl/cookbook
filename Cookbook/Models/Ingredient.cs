﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cookbook.Models
{
    public class Ingredient : ModelBase
    {
        [Required]
        public string Name { get; set; }

        [ForeignKey(nameof(BaseIngredient))]
        public int? BaseIngredientId { get; set; }
        public virtual Ingredient BaseIngredient { get; set; }

        public virtual ICollection<RecipesIngredientAmount> AmountsInRecipes { get; set; }

        public virtual ICollection<IngredientsGroupsIngredientAmount> AmountsInGroups { get; set; }

        public virtual ICollection<Ingredient> DetailedIngredients { get; set; }
    }
}
