﻿using Cookbook.CustomValidators;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cookbook.Models
{
    [XorRequired(nameof(Amount), nameof(AmountMin))]
    [XnorRequired(nameof(AmountMin), nameof(AmountMax))]
    public abstract class IngredientAmountBase : ModelBase
    {
        public double? Amount { get; set; }

        public double? AmountMin { get; set; }

        public double? AmountMax { get; set; }

        [ForeignKey(nameof(Unit))]
        public int? UnitId { get; set; }
        public virtual Unit Unit { get; set; }

        [ForeignKey(nameof(Ingredient)), Required]
        public int IngredientId { get; set; }
        public virtual Ingredient Ingredient { get; set; }
    }
}
