﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cookbook.Models
{
    public class IngredientsGroup : ModelBase
    {
        [Required]
        public string Name { get; set; }

        [ForeignKey(nameof(RecipePoint)), Required]
        public int RecipePointId { get; set; }
        public virtual RecipePoint RecipePoint { get; set; }

        public virtual ICollection<IngredientsGroupsIngredientAmount> IngredientsAmounts { get; set; }
    }
}
