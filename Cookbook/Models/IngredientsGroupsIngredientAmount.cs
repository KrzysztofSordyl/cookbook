﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cookbook.Models
{
    public class IngredientsGroupsIngredientAmount : IngredientAmountBase
    {
        [ForeignKey(nameof(IngredientsGroup)), Required]
        public int IngredientsGroupId { get; set; }
        public virtual IngredientsGroup IngredientsGroup { get; set; }
    }
}
