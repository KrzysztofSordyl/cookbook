﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cookbook.Models
{
    public class Recipe : ModelBase
    {
        [Required]
        public string Name { get; set; }

        public string Description { get; set; }

        [Required]
        public int PreparationTime { get; set; }

        public virtual ICollection<RecipePoint> Points { get; set; }

        public virtual ICollection<RecipesIngredientAmount> IngredientAmounts { get; set; }
    }
}
