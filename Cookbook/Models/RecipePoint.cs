﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cookbook.Models
{
    public class RecipePoint : ModelBase
    {
        public string Name { get; set; }
        
        public int? PreparationTime { get; set; }

        [Required]
        public int Number { get; set; }

        [Required]
        public string Text { get; set; }

        [ForeignKey(nameof(Recipe)), Required]
        public int RecipeId { get; set; }
        public virtual Recipe Recipe { get; set; }

        public virtual ICollection<IngredientsGroup> IngredientsGroups { get; set; }
    }
}
