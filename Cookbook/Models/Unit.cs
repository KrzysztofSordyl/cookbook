﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cookbook.Models
{
    public class Unit : ModelBase
    {
        [Required]
        public string Name { get; set; }

        public virtual ICollection<RecipesIngredientAmount> RecipesIngredientAmounts { get; set; }

        public virtual ICollection<IngredientsGroupsIngredientAmount> IngredientsGroupsIngredientAmounts { get; set; }
    }
}
