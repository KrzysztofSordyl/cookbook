﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Optimization;

namespace CookbookWeb
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Scripts/jquery/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                "~/Scripts/jquery/jquery.unobtrusive*",
                "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                "~/Scripts/modernizr/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                "~/Scripts/bootstrap/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                 "~/Content/bootstrap.css",
                 "~/Content/Site.css"));

            bundles.Add(new ScriptBundle("~/bundles/lodash").Include(
                "~/Scripts/lodash/lodash.js"));

            // ### <VUE> ###
            bundles.Add(new ScriptBundle("~/bundles/vue").Include(
                "~/Scripts/vue/vue.js"));

            bundles.Add(new ScriptBundle("~/bundles/vueapp").IncludeDirectory(
                "~/Scripts/app/components", "*.js", true).IncludeDirectory(
                "~/Scripts/app/viewmodels", "*.js", true).Include(
                "~/Scripts/app/common.js",
                "~/Scripts/app/cookbook-api.js",
                "~/Scripts/app/app.js"));
        }
    }
}
