﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Cookbook.CookbookDatabase;
using Cookbook.Models;
using CookbookWeb.Models.RecipeViewModels;

namespace CookbookWeb.Controllers
{
    public class RecipesController : ApiController
    {
        private CookbookDbContext db = new CookbookDbContext();

        // GET: api/Recipes
        public List<RecipeViewModel> GetRecipes()
        {
            return db.Recipes
                .ToList()
                .Select(r => (RecipeViewModel)r)
                .ToList();
        }

        // GET: api/Recipes/5
        [ResponseType(typeof(RecipeDetailsViewModel))]
        public IHttpActionResult GetRecipe(int id)
        {
            var recipe = (RecipeDetailsViewModel)db.Recipes
                .Include(r => r.IngredientAmounts.Select(ia => ia.Unit))
                .Include(r => r.IngredientAmounts.Select(ia => ia.Ingredient))
                .Include(r => r.Points.Select(rp => rp.IngredientsGroups.Select(ig => ig.IngredientsAmounts.Select(ia => ia.Unit))))
                .Include(r => r.Points.Select(rp => rp.IngredientsGroups.Select(ig => ig.IngredientsAmounts.Select(ia => ia.Ingredient))))
                .Where(r => r.Id == id)
                .SingleOrDefault();
            if (recipe == null)
            {
                return NotFound();
            }

            return Ok(recipe);
        }

        // PUT: api/Recipes/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutRecipe(int id, Recipe recipe)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != recipe.Id)
            {
                return BadRequest();
            }

            db.Entry(recipe).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RecipeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Recipes
        [ResponseType(typeof(Recipe))]
        public IHttpActionResult PostRecipe(Recipe recipe)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Recipes.Add(recipe);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = recipe.Id }, recipe);
        }

        // DELETE: api/Recipes/5
        [ResponseType(typeof(Recipe))]
        public IHttpActionResult DeleteRecipe(int id)
        {
            Recipe recipe = db.Recipes.Find(id);
            if (recipe == null)
            {
                return NotFound();
            }

            db.Recipes.Remove(recipe);
            db.SaveChanges();

            return Ok(recipe);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool RecipeExists(int id)
        {
            return db.Recipes.Count(e => e.Id == id) > 0;
        }
    }
}