﻿using Cookbook.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CookbookWeb.Models.RecipeViewModels
{
    public class RecipeDetailsViewModel : RecipeViewModel
    {
        public RecipeDetailsViewModel()
        {
        }

        public RecipeDetailsViewModel(int id, string name, string description, int preparationTime, List<IngredientAmountViewModel> ingredients, List<RecipePointViewModel> points) : base(id, name, description, preparationTime)
        {
            Ingredients = ingredients;
            Points = points;
        }

        public List<IngredientAmountViewModel> Ingredients { get; set; }

        public List<RecipePointViewModel> Points { get; set; }

        public static explicit operator RecipeDetailsViewModel(Recipe recipe)
        {
            if (recipe == null)
                return null;

            return new RecipeDetailsViewModel(
                recipe.Id,
                recipe.Name,
                recipe.Description,
                recipe.PreparationTime,
                recipe.IngredientAmounts
                .Select(ia => (IngredientAmountViewModel)ia)
                .ToList(),
                recipe.Points
                .Select(p => (RecipePointViewModel)p)
                .ToList());
        }
    }

    public class IngredientAmountViewModel
    {
        public IngredientAmountViewModel()
        {
        }

        public IngredientAmountViewModel(int id, double? amount, double? amountMin, double? amountMax, IngredientViewModel ingredient, UnitViewModel unit)
        {
            Id = id;
            Amount = amount;
            AmountMin = amountMin;
            AmountMax = amountMax;
            Ingredient = ingredient;
            Unit = unit;
        }

        public int Id { get; set; }

        public double? Amount { get; set; }

        public double? AmountMin { get; set; }

        public double? AmountMax { get; set; }

        public IngredientViewModel Ingredient { get; set; }

        public UnitViewModel Unit { get; set; }

        public static explicit operator IngredientAmountViewModel(IngredientAmountBase amount)
        {
            if (amount == null)
                return null;

            return new IngredientAmountViewModel(
                amount.Id,
                amount.Amount,
                amount.AmountMin,
                amount.AmountMax,
                (IngredientViewModel)amount.Ingredient,
                (UnitViewModel)amount.Unit);
        }
    }

    public class IngredientViewModel
    {
        public IngredientViewModel()
        {
        }

        public IngredientViewModel(int id, string name)
        {
            Id = id;
            Name = name;
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public static explicit operator IngredientViewModel(Ingredient ingredient)
        {
            if (ingredient == null)
                return null;

            return new IngredientViewModel(
                ingredient.Id,
                ingredient.Name);
        }
    }

    public class UnitViewModel
    {
        public UnitViewModel()
        {
        }

        public UnitViewModel(int id, string name)
        {
            Id = id;
            Name = name;
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public static explicit operator UnitViewModel(Unit unit)
        {
            if (unit == null)
                return null;

            return new UnitViewModel(
                unit.Id,
                unit.Name);
        }
    }

    public class RecipePointViewModel
    {
        public RecipePointViewModel()
        {
        }

        public RecipePointViewModel(int id, int number, string name, int? preparationTime, string text, List<IngredientsGroupViewModel> ingredientsGroups)
        {
            Id = id;
            Number = number;
            Name = name;
            PreparationTime = preparationTime;
            Text = text;
            IngredientsGroups = ingredientsGroups;
        }

        public int Id { get; set; }

        public int Number { get; set; }

        public string Name { get; set; }

        public int? PreparationTime { get; set; }

        public string Text { get; set; }

        public List<IngredientsGroupViewModel> IngredientsGroups { get; set; }

        public static explicit operator RecipePointViewModel(RecipePoint point)
        {
            if (point == null)
                return null;

            return new RecipePointViewModel(
                point.Id,
                point.Number,
                point.Name,
                point.PreparationTime,
                point.Text,
                point.IngredientsGroups
                .Select(ig => (IngredientsGroupViewModel)ig)
                .ToList());
        }
    }

    public class IngredientsGroupViewModel
    {
        public IngredientsGroupViewModel()
        {
        }

        public IngredientsGroupViewModel(int id, string name, List<IngredientAmountViewModel> ingredients)
        {
            Id = id;
            Name = name;
            Ingredients = ingredients;
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public List<IngredientAmountViewModel> Ingredients { get; set; }

        public static explicit operator IngredientsGroupViewModel(IngredientsGroup group)
        {
            if (group == null)
                return null;

            return new IngredientsGroupViewModel(
                group.Id,
                group.Name,
                group.IngredientsAmounts
                .Select(ia => (IngredientAmountViewModel)ia)
                .ToList());
        }
    }
}