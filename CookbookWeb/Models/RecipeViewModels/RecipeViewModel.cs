﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Cookbook.Models;

namespace CookbookWeb.Models.RecipeViewModels
{
    public class RecipeViewModel
    {
        public RecipeViewModel()
        {
        }

        public RecipeViewModel(int id, string name, string description, int preparationTime)
        {
            Id = id;
            Name = name;
            Description = description;
            PreparationTime = preparationTime;
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int PreparationTime { get; set; }

        public static implicit operator RecipeViewModel(Recipe recipe)
        {
            return new RecipeViewModel(
                recipe.Id,
                recipe.Name,
                recipe.Description,
                recipe.PreparationTime);
        }
    }
}