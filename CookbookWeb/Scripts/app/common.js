﻿window.common = (function () {
    var common = {};

    common.getFragment = function getFragment() {
        if (window.location.hash.indexOf("#") === 0) {
            return parseQueryString(window.location.hash.substr(1));
        } else {
            return {};
        }
    };

    function parseQueryString(queryString) {
        var data = {},
            pairs, pair, separatorIndex, escapedKey, escapedValue, key, value;

        if (queryString === null) {
            return data;
        }

        pairs = queryString.split("&");

        for (var i = 0; i < pairs.length; i++) {
            pair = pairs[i];
            separatorIndex = pair.indexOf("=");

            if (separatorIndex === -1) {
                escapedKey = pair;
                escapedValue = null;
            } else {
                escapedKey = pair.substr(0, separatorIndex);
                escapedValue = pair.substr(separatorIndex + 1);
            }

            key = decodeURIComponent(escapedKey);
            value = decodeURIComponent(escapedValue);

            data[key] = value;
        }

        return data;
    }

    common.setAccessToken = function (accessToken) {
        sessionStorage.setItem("accessToken", accessToken);
    };

    common.getAccessToken = function () {
        var token = sessionStorage.getItem("accessToken");

        if (!token) {
            var fragment = common.getFragment();
            if (fragment.access_token) {
                window.location.hash = fragment.state || '';
                common.setAccessToken(fragment.access_token);
                token = fragment.access_token;
            } else {
                window.location = "/Account/Authorize?client_id=web&response_type=token&state=" + encodeURIComponent(window.location.hash);
            }
        }
        return token;
    };

    common.getAccessToken();

    return common;
})();