﻿Vue.component("ingredient-list", {
    data: function () {
        return {};
    },
    props: ["ingredients"],
    template: document.getElementById("ingredientListComponent").innerHTML
});