﻿Vue.component("ingredients-group", {
    data: function () {
        return {};
    },
    props: ["ingredients", "name"],
    template: document.getElementById("ingredientsGroupComponent").innerHTML
});