﻿Vue.component("recipe-details", {
    data: function () {
        return {
            recipe: {}
        };
    },
    props: ["id"],
    created: function () {
        CookbookApi.Recipes.Get(data => {
            this.recipe = data;
        }, this.id);
    },
    template: document.getElementById("recipeDetailsComponent").innerHTML
});