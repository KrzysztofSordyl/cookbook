﻿Vue.component("recipe-list", {
    data: function () {
        return {
            recipes: []
        };
    },
    created: function () {
        CookbookApi.Recipes.GetList(data => {
            this.recipes = data;
        });
    },
    template: document.getElementById("recipeListComponent").innerHTML
});