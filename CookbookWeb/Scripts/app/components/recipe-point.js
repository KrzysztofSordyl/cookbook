﻿Vue.component("recipe-point", {
    data: function () {
        return {};
    },
    props: ["point"],
    template: document.getElementById("recipePointComponent").innerHTML
});