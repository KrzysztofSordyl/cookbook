﻿Vue.component("recipe", {
    props: ["recipe"],
    template: document.getElementById("recipeComponent").innerHTML
});