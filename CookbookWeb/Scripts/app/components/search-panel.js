﻿Vue.component("search-panel", {
    data: function () {
        return {
            searchInput: ""
        };
    },
    watch: {
        searchInput: function (newValue, oldValue) {
            this.searchInputChanged(newValue, oldValue);
        }
    },
    created: function () {
        this.searchInputChanged = _.debounce(function (newValue, oldValue) {
                this.$emit('search-input-changed', newValue, oldValue);
            }, 500);
    },
    template: document.getElementById("searchPanelComponent").innerHTML
});