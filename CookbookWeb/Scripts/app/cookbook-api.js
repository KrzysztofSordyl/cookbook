﻿var CookbookApi = (function () {
    var apiHelper = {
        get: function (controller, onSuccess, id = null) {
            let url = "/api/" + controller;
            if (id)
                url = url + "/" + id;
            $.ajax({
                method: "get",
                url: url,
                contentType: "application/json; charset=utf-8",
                headers: {
                    "Authorization": "Bearer " + window.common.getAccessToken()
                },
                success: onSuccess,
                error: function (xhr) { alert(xhr.responseText); }
            });
        }
    };

    var cookbookapi = {
        Account: {
            helper: apiHelper,
            GetHometown: function (onSuccess) {
                this.helper.get("Me", onSuccess);
            }
        },
        Recipes: {
            helper: apiHelper,
            GetList: function (onSuccess) {
                this.helper.get("Recipes", onSuccess);
            },
            Get: function (onSuccess, id) {
                this.helper.get("Recipes", onSuccess, id);
            }
        }
    };

    return cookbookapi;
})();