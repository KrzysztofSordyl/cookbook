﻿var InitializeCookbookViewModel = () => {
    var CookbookViewModel = new Vue({
        el: "#cookbook",
        data: {
            currentView: "recipe-list",
            id: 0
        },
        methods: {
            handleSearchInputChanged: function (newValue, oldValue) {
                
            },
            handleRecipeSelected: function (id) {
                this.currentView = "recipe-details";
                this.id = id;
            }
        }
    });
}