﻿var InitializeVueTestViewModel = () => {
    new Vue({
        el: "#testVue",
        data: {
            hometown: "No information has been downloaded."
        },
        mounted: function () {
            CookbookApi.Account.GetHometown(
                data => {
                    this.hometown = data.hometown;
                });
        }
    });
}